import scrapy

from crawler.items import BusinessItem

class QuotesSpider(scrapy.Spider):
    name = "quotes2"
    custom_settings = {
        'ITEM_PIPELINES': {
            'crawler.pipelines.BusinessPipeline': 100,
        }
    }
    counter = 1
    # allowed_domains = ["linkbusiness.ph"]
    start_urls = [
        "https://linkbusiness.ph/businesses-for-sale/search?sortBy=FeaturedFirst&page=1&searchByName=True&commissionSplit=AllListings"
    ]

    def parse(self, response):
        num_of_items = len(response.css('div.card'))

        for item_index in range(num_of_items):
            card = response.css('div#bcid_11955668')[item_index]
            info = card.css('div.p-3.vertical-listing')

            i = BusinessItem()
            i['listing_id'] = (card.css('div.card-footer span::text').get()).strip().split(' ')[1]
            i['title'] = info.css('h3 a::text').get()
            i['price'] = info.css('p.price a::text').get()
            i['link'] = card.css('div.show-if-not-iframe a::attr(href)').extract_first()
            i['profit'] = info.css('p.sub-price')[0].css('a::text').get()
            i['sales'] = info.css('p.sub-price')[1].css('a::text').get()
            i['location'] = info.css('p.location a::text').get()
            i['description'] = ''
            i['sold'] = False
            i['version'] = 0

            yield i

        there_is_nxt_page = response.css('ul.pagination.webapp li.pag-next a::text').get()

        if there_is_nxt_page:
            self.counter += 1
            nxt_page = f"https://linkbusiness.ph/businesses-for-sale/search?sortBy=FeaturedFirst&page={self.counter}&searchByName=True&commissionSplit=AllListings"
            yield scrapy.Request(url=nxt_page, callback=self.parse)
