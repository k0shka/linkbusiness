import scrapy
import random

class QuotesSpider(scrapy.Spider):
    name = "quotes"
    start_urls = [
        "https://linkbusiness.ph/businesses-for-sale/search?sortBy=FeaturedFirst&page=1&searchByName=True&commissionSplit=AllListings",
        "https://linkbusiness.ph/businesses-for-sale/search?sortBy=FeaturedFirst&page=2&searchByName=True&commissionSplit=AllListings",
    ]
    # def start_requests(self):
    #     urls = [
    #         "https://linkbusiness.ph/businesses-for-sale/search?sortBy=FeaturedFirst&page=1&searchByName=True&commissionSplit=AllListings",
    #         "https://linkbusiness.ph/businesses-for-sale/search?sortBy=FeaturedFirst&page=2&searchByName=True&commissionSplit=AllListings",
    #     ]
    #
    #     for url in urls:
    #         yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        # page = response.url.split("/")[-2]
        page = random.randint(0,9)
        filename = 'biz-%s.html' % page
        with open(filename, 'wb') as f:
            f.write(response.body)
        self.log('Saved file %s' % filename)