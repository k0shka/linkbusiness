import scrapy

from crawler.items import OfficeItem

class GetOfficesSpider(scrapy.Spider):
    name = "get-offices"
    custom_settings = {
        'ITEM_PIPELINES': {
            'crawler.pipelines.OfficePipeline': 100,
        }
    }
    start_urls = [
        "https://linkbusiness.ph/contact-us"
    ]

    def parse(self, response):
        officeTexts = response.xpath('//select[@id="OfficeId"]/option/text()').getall()
        officeValues = response.xpath('//select[@id="OfficeId"]/option/@value').getall()
        if len(officeTexts) == len(officeValues):
            counter = 0
            for option_value in officeValues:
                counter += 1
                if option_value == '':
                    continue

                i = OfficeItem()
                i['office_id'] = option_value
                i['name'] = officeTexts[counter-1]

                yield i
