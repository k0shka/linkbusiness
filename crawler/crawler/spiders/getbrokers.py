import scrapy
import requests
from bs4 import BeautifulSoup
from crawler.items import BrokerItem
from app.models import Office

from scrapy import FormRequest


class GetBrokersSpider(scrapy.Spider):
    name = "get-brokers"
    custom_settings = {
        'ITEM_PIPELINES': {
            'crawler.pipelines.BrokerPipeline': 100,
        }
    }
    start_urls = [
        "https://linkbusiness.ph/contact-us"
    ]

    def parse(self, response):

        offices = Office.objects.all()

        for office in offices:
            token = response.xpath('//*[@name="__RequestVerificationToken"]/@value').extract_first()
            payload = {
                'OfficeId': office.office_id,
                'BrokerName': '',
                '__RequestVerificationToken': token
            }

            request = FormRequest('https://linkbusiness.ph/ContactUs/BrokersSearchResults', method='POST',
                                  formdata=payload,
                                  callback=self.finished)
            request.meta['office'] = office
            yield request

    def finished(self, response):
        parent_div = response.css('div.webappsearchresults')
        num_of_items = len(parent_div.css('div.team-member'))
        office = response.meta['office']
        for item_index in range(num_of_items):
            team_details = parent_div.css('div.team-member div.teamdetails')[item_index]
            i = BrokerItem()
            i['fullname'] = team_details.css('a.member-name::text').get()
            i['phone'] = team_details.css('div.member-phone span.infoValue::text').get()
            i['mobile'] = team_details.css('div.member-mobile span.infoValue::text').get()
            i['email'] = team_details.css('div.member-email a.infoValue::text').get()
            i['office'] = office
            yield i
