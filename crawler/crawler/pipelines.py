# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from decimal import Decimal
import sys
from itemadapter import ItemAdapter

from app.models import Business, Office, Country, Broker
import re


def clear_text(param):
    return param.strip()


def xtract_nums_from_curval(s):
    d = {}
    result = re.findall(r'(?:[\£\$\€\₱]{1}[,\d]+.?\d*)', s)
    if len(result) <= 0:
        money = s
    else:
        money = result[0]
    value = Decimal(re.sub(r'[^\d.]', '', money))
    # print(value)
    d['value'] = str(value)
    list_string = s.split(' ')
    list_string.pop(0)
    remark = ' '.join(list_string)
    d['remark'] = remark
    return d


class BusinessPipeline(list):
    def process_item(self, item, spider):
        item['title'] = clear_text(item['title'])
        item['listing_id'] = clear_text(item['listing_id'])
        if item['price'] != 'Refer to Broker' and item['price'] != '' and item['price'] is not None:
            process_price = xtract_nums_from_curval(item['price'])
            price = int(process_price['value'])
            price_detail = clear_text(process_price['remark'])
        else:
            price = Decimal('0.00')
            price_detail = ''
        if item['sales'] != 'Refer to Broker' and item['sales'] != '' and item['sales'] is not None:
            process_sales = xtract_nums_from_curval(item['sales'])
            item['sales'] = process_sales['value']
        if item['profit'] != 'Refer to Broker' and item['profit'] != '' and item['profit'] is not None:
            process_sales = xtract_nums_from_curval(item['profit'])
            item['profit'] = process_sales['value']

        try:
            obj, created = Business.objects.update_or_create(
                listing_id=item['listing_id'],
                defaults={
                    'listing_id': item['listing_id'],
                    'title': item['title'],
                    'price': price,
                    'link': item['link'],
                    'price_detail': price_detail,
                    'profit': item['profit'],
                    'sales': item['sales'],
                    'location': item['location'],
                    'sold': item['sold'],
                    'version': item['version'],
                }

            )
        except:
            return 'ERROR: ' + item['listing_id']

        return item

class BrokerPipeline(object):
    def process_item(self, item, spider):
        try:
            obj, created = Broker.objects.update_or_create(
                fullname=item['fullname'],
                defaults={
                    'fullname': item['fullname'],
                    'phone': item['phone'],
                    'mobile': item['mobile'],
                    'email': item['email'],
                    'office': item['office']
                }
            )
        except Exception as details:
            return "UNEXPECTED ERROR: {0}".format(details)

        return item


class OfficePipeline(object):
    def process_item(self, item, spider):
        try:
            country = Country.objects.get(code='PH')
            obj, created = Office.objects.update_or_create(
                office_id=item['office_id'],
                defaults={
                    'office_id': item['office_id'],
                    'name': item['name'],
                    'country': country,
                }
            )
        except Exception as details:
            return "UNEXPECTED ERROR: {0}".format(details)

        return item
