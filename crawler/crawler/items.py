# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy_djangoitem import DjangoItem
from app.models import Business, Office, Broker

class BusinessItem(DjangoItem):
    django_model = Business

class OfficeItem(DjangoItem):
    django_model = Office

class BrokerItem(DjangoItem):
    django_model = Broker

class CrawlerItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass
