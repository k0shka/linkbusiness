from datetime import datetime

from django.db import models


class Business(models.Model):
    listing_id = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    price = models.DecimalField(max_digits=15, decimal_places=2)
    link = models.CharField(max_length=200, null=True)
    price_detail = models.CharField(max_length=200, null=True, blank=True)
    profit = models.CharField(max_length=100, null=True, blank=True)
    sales = models.CharField(max_length=100, null=True, blank=True)
    location = models.CharField(max_length=150, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    poster = models.ImageField('cover', blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True, null=True)
    updated_on = models.DateTimeField(auto_now=True)
    removed = models.BooleanField(default=False)
    sold = models.BooleanField(default=False)
    version = models.IntegerField(default=0)
    version_new = models.IntegerField(null=True, blank=True)
    version_sold = models.IntegerField(null=True, blank=True)
    version_withdraw = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.title

    def created(self):
        time = datetime.now()
        if self.created_on.day == time.day:
            return str(time.hour - self.created_on.hour) + " hours ago"
        else:
            if self.created_on.month == time.month:
                return str(time.day - self.created_on.day) + " days ago"
            else:
                if self.created_on.year == time.year:
                    return str(time.month - self.created_on.month) + " months ago"
        return self.created_on

    def updated(self):
        time = datetime.now()
        if self.updated_on.day == time.day:
            return str(time.hour - self.updated_on.hour) + " hours ago"
        else:
            if self.updated_on.month == time.month:
                return str(time.day - self.updated_on.day) + " days ago"
            else:
                if self.updated_on.year == time.year:
                    return str(time.month - self.updated_on.month) + " months ago"
        return self.updated_on

    class Meta:
        verbose_name_plural = "businesses"


class Industry(models.Model):
    title = models.CharField(max_length=100, db_index=True, unique=True)
    businesses = models.ForeignKey(Business, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "industries"


class BusinessType(models.Model):
    title = models.CharField(max_length=100, db_index=True, unique=True)
    businesses = models.ForeignKey(Business, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "business types"


class Country(models.Model):
    name = models.CharField(max_length=60)
    code = models.CharField(max_length=3)

    def __str__(self):
        return self.name + ' (' + self.code + ')';

    class Meta:
        verbose_name_plural = "countries"


class Office(models.Model):
    office_id = models.CharField(max_length=120, blank=True)
    name = models.CharField(max_length=64)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True, null=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "offices"


class Broker(models.Model):
    fullname = models.CharField(max_length=128)
    phone = models.CharField(max_length=64, blank=True)
    mobile = models.CharField(max_length=64, blank=True)
    email = models.CharField(max_length=64)
    linkedin = models.CharField(max_length=64, blank=True)
    # relationships
    businesses = models.ForeignKey(Business, null=True, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True, null=True)
    updated_on = models.DateTimeField(auto_now=True)
    office = models.ForeignKey(Office, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.fullname

    class Meta:
        verbose_name_plural = "brokers"


class UserGeoIP(models.Model):
    pass


class ScrapeVersion(models.Model):
    pass


class Movie(models.Model):
    title = models.CharField(max_length=255, unique=True)
    critics_consensus = models.TextField(blank=True, null=True)
    average_grade = models.DecimalField(max_digits=3, decimal_places=2, blank=True, null=True)
    poster = models.ImageField(blank=True, null=True)
    amount_reviews = models.PositiveIntegerField(blank=True, null=True)
    approval_percentage = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return self.title
