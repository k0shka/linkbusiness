# Generated by Django 3.1.1 on 2020-09-29 10:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20200929_1753'),
    ]

    operations = [
        migrations.AlterField(
            model_name='business',
            name='version_new',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='business',
            name='version_sold',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='business',
            name='version_withdraw',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
