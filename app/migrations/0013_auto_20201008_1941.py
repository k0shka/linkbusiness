# Generated by Django 3.1.2 on 2020-10-08 11:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0012_broker_updated_on'),
    ]

    operations = [
        migrations.AlterField(
            model_name='broker',
            name='businesses',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='app.business'),
        ),
        migrations.AlterField(
            model_name='broker',
            name='office',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='app.office'),
        ),
    ]
