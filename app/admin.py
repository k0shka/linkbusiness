from django.contrib import admin
from .models import (Business, Industry, BusinessType,
                     Country, Office, Broker)


class BusinessAdmin(admin.ModelAdmin):
    list_display = ('listing_id', 'title', 'created', 'updated')


admin.site.register(Business, BusinessAdmin)
admin.site.register(Industry)
admin.site.register(BusinessType)
admin.site.register(Country)
admin.site.register(Office)
admin.site.register(Broker)
