from django.urls import path

from app.views import BusinessListView, test_scraper, test_scraper_json

urlpatterns = [
    path('', BusinessListView.as_view(), name='businesses'),
    path('scrape', test_scraper, name="test-scraper"),
    path('scraper-json', test_scraper_json, name="test-scraper-json"),
]