main_url = 'https://linkbusiness.ph'
pre_url = 'https://linkbusiness.ph/businesses-for-sale/search?sortBy=LatestListing&page='
post_url = '&searchByName=True&commissionSplit=AllListings'
broker_url = 'https://linkbusiness.ph/ContactUs/BrokersSearchResults'
contactus_url = '/contact-us'
broker_listing_url = 'https://linkbusiness.ph/businesses-for-sale/broker/'
or_office = '4f745359-676a-e611-80f3-c4346bac339c'
al_office = 'cbbc5e7e-c5bb-e811-81d6-e0071b6a71b1'

main_url_us = 'https://linkbusiness.com'
pre_url_us = main_url_us + '/businesses-for-sale/search?sortBy=LatestListing&page='
broker_listing_url_us = main_url_us + '/businesses-for-sale/broker/'
broker_url_us = main_url_us + '/ContactUs/BrokersSearchResults'

main_url_nz = 'https://linkbusiness.co.nz'
pre_url_nz = main_url_nz + '/businesses-for-sale/search?sortBy=LatestListing&page='
broker_listing_url_nz = main_url_nz + '/businesses-for-sale/broker/'
broker_url_nz = main_url_nz + '/ContactUs/BrokersSearchResults'

main_url_au = 'https://linkbusiness.com.au'
pre_url_au = main_url_au + '/businesses-for-sale/search?sortBy=LatestListing&page='
broker_listing_url_au = main_url_au + '/businesses-for-sale/broker/'
broker_url_au = main_url_au + '/ContactUs/BrokersSearchResults'