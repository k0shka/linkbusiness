from app.utils.urls import (
    main_url,
    main_url_us,
    main_url_nz,
    main_url_au,
    pre_url,
    pre_url_us,
    pre_url_nz,
    pre_url_au,
    post_url,
    broker_url,
    broker_url_us,
    broker_url_nz,
    broker_url_au,
    contactus_url,
    broker_listing_url,
    broker_listing_url_us,
    broker_listing_url_nz,
    broker_listing_url_au,
    or_office,
    al_office,
)

from bs4 import BeautifulSoup
import requests
import sys
def test_scrape():
    base_url = pre_url_us + '1' + post_url
    r = requests.get(base_url)
    soup = BeautifulSoup(r.text, "html.parser")
    item_count = soup.find('div', {'class':'footer-showing-result'})
    # value = get_numeric(item_count.find('span').text)
    return item_count.find('span').text


def scrape_card_listing(country_code):
    sys.setrecursionlimit(1500)
    # get last page number
    pre_url_str = ''
    mainUrl = ''
    if country_code == 'PH':
        pre_url_str = pre_url
        mainUrl = main_url
    elif country_code == 'US':
        pre_url_str = pre_url_us
        mainUrl = main_url_us
    elif country_code == 'NZ':
        pre_url_str = pre_url_nz
        mainUrl = main_url_nz
    elif country_code == 'AU':
        pre_url_str = pre_url_au
        mainUrl = main_url_au
    print("Scraping data...")
    base_url = pre_url_str + '1' + post_url
    r = requests.get(base_url)
    soup = BeautifulSoup(r.text, "html.parser")
    item_count = soup.find('div', {'class': 'footer-showing-result'})
    value = get_numeric(item_count.find('span').text)
    last_page = int(value / 24)
    if value % 24 != 0:
        last_page = last_page + 1

    l = []
    last_page = 2
    business_count = 0
    running = True
    for page in range(0, last_page):
        page = page + 1
        base_url = pre_url_str + str(page) + post_url
        # Request URL and Beautiful Parser
        r = requests.get(base_url)
        soup = BeautifulSoup(r.text, "html.parser")

        all_biz = soup.find_all('div', {"id": "bcid_11955668"})
        business_count = business_count + len(all_biz)
        try:
            if len(all_biz) <= 0:
                print(base_url)
                print(str(len(all_biz)))
                break
            for biz in all_biz:
                d = {}
                # scrape listing detail from card view

                pr = biz.find("div", {"class": "position-relative"})
                if pr is not None:
                    d['sold'] = True
                else:
                    d['sold'] = False

                biz_info = biz.find("div", {"class": "p-3 vertical-listing"})
                biz_footer = biz.find("div", {"class": "card-footer"})
                biz_footer_div = biz_footer.find('div')

                try:
                    d['biz_name'] = biz_info.find('a').contents[0]
                except:
                    d['biz_name'] = '<none>'

                price_info = biz_info.find("p", {"class": "price"})
                try:
                    d['price_info'] = price_info.find("a").contents[0]
                except (IndexError, AttributeError) as e:
                    d['price_info'] = 'Refer to Broker'
                sub_link = biz_footer_div.a['href']
                biz_link = mainUrl + sub_link
                try:
                    d['biz_link'] = biz_link
                except (IndexError, AttributeError) as e:
                    d['biz_link'] = '<none>'
                try:
                    d['listing_id'] = sub_link.split('/')[2]
                except (IndexError, AttributeError) as e:
                    d['listing_id'] = '<none>'
                try:
                    d['details'] = 'scrape_listing_details(biz_link)'
                except (IndexError, AttributeError) as e:
                    d['details'] = "<none>"
                ### PH
                try:
                    sr = biz.find("span", text="Sales Revenue:")
                    d['sales_detail'] = sr.find_parent('p').find('a').contents[0]
                except (IndexError, AttributeError) as e:
                    d['sales_detail'] = "<none>"
                try:
                    pd = biz.find("span", text="Profit*:")
                    d['profit_detail'] = pd.find_parent('p').find('a').contents[0]
                except (IndexError, AttributeError) as e:
                    d['profit_detail'] = "<none>"

                try:
                    pr = biz.find("span", text="Price:")
                    d['price_detail'] = pr.find_parent('p').find('a').contents[0]
                except (IndexError, AttributeError) as e:
                    d['price_detail'] = "<none>"
                d['office_detail'] = ''
                d['description'] = ''
                try:
                    loc = biz.find("span", text="Location: ")
                    location = loc.find_parent('p').find('a').contents[0]
                    d['location_detail'] = word_trimmer(location)
                except (IndexError, AttributeError) as e:
                    d['location_detail'] = "<none>"
                d['broker_name'] = ''
                try:
                    ind = biz.find("span", text="Industry: ")
                    industry = ind.find_parent('p').find('a').contents[0]
                    d['industry_detail'] = word_trimmer(industry)
                except (IndexError, AttributeError) as e:
                    d['industry_detail'] = "<none>"
                try:
                    typ = biz.find("span", text="Type: ")
                    typeD = typ.find_parent('p').find('a').contents[0]
                    d['type_detail'] = word_trimmer(typeD)
                except (IndexError, AttributeError) as e:
                    d['type_detail'] = "<none>"
                ### PH
                l.append(d)
        except (IndexError, AttributeError) as e:
            print(str(len(all_biz)))
            print('failed')
            break

    return l

def word_trimmer(str):
    if str.find('business') != -1:
        x = str.split(" business", 2)
        return x[0]
    else:
        if str.find('freehold') != -1:
            x = str.split(" freehold", 2)
            return x[0]
        else:
            return str

def get_numeric(str):
    x = str.split(' items.', 2)
    return int(x[0])