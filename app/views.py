from django.http import HttpResponse, JsonResponse
from django.views.generic import ListView

from app.models import Business
from app.utils.scraper import test_scrape, scrape_card_listing

class BusinessListView(ListView):
    model = Business
    template_name = 'business_list.html'
    # context_object_name = 'all_business_list'

def test_scraper(request):
    return HttpResponse(test_scrape())

def test_scraper_json(request):
    print(test_scraper_json('PH'))
    return JsonResponse(['test', 'test'], safe=False)